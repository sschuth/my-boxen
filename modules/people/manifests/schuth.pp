class people::schuth {
  include people::schuth::cli
  include people::schuth::databases
  include people::schuth::osxsettings
  include people::schuth::applications
  include people::schuth::homebrew
  include people::schuth::dotfiles
  include people::schuth::privateconfiguration
  include people::schuth::gitsetup
  include people::schuth::lhprojectsetup
}