class people::schuth::homebrew{
  
	
	package {
		[
		  'figlet',
      'maven',
      'ant',
      'reattach-to-user-namespace',
      'tmux',
      'tig'
    ]:
    ensure => 'latest'
	}

  #package { 'tomcat':
  #  ensure => '7.0.53',
  #  require => Homebrew::Tap['homebrew/versions']
  #}
  
  # configure maven options
  file_line { 'set maven_opts':
    path    => "/Users/$::luser/.zshrc",
    line    => "export MAVEN_OPTS='-Xmx2048m -XX:MaxPermSize=512m'",
  }
}