class people::schuth::applications{
	
	# IntelliJ
	class { 'intellij':
  	edition => 'ultimate',
   	version => '13.1.5'
  }
  
  # Chrome
  include chrome::beta

  # Sublime text
  include sublime_text

  # SourceTree
  include sourcetree

  #Spotify
  include spotify
  
  #iTerm 2
  include iterm2::stable
  include iterm2::colors::solarized_dark

  #LaunchBar 
  include launchbar


}