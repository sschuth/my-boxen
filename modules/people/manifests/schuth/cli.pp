class people::schuth::cli {

  osx_chsh { $::luser:
    shell    => '/bin/zsh',
  }

  file_line { 'source .profile within .zshrc':
    path    => "/Users/$::luser/.zshrc",
    line    => "source /Users/$::luser/.profile",
  }

  # use oh-my-zsh
  include ohmyzsh
}