class people::schuth::dotfiles {

	repository { "/Users/${::luser}/github/dotfiles":
    source => 'schuth/configurations',
  }

  file { "/Users/${::luser}/.tmux.conf":
    ensure  => link,
    mode    => '0644',
    target  => "/Users/${::luser}/github/dotfiles/tmux/tmux.conf",
    require => Repository["/Users/${::luser}/github/dotfiles"],
  }

  file { "/Users/${::luser}/.vimrc":
    ensure  => link,
    mode    => '0644',
    target  => "/Users/${::luser}/github/dotfiles/vim/vimrc",
    require => Repository["/Users/${::luser}/github/dotfiles"],
  }

  file { "/Users/${::luser}/.vim":
    ensure  => link,
    mode    => '0644',
    target  => "/Users/${::luser}/github/dotfiles/vim",
    require => Repository["/Users/${::luser}/github/dotfiles"],
  }
  
  file { "/Users/${::luser}/.zshrc":
    ensure  => link,
    mode    => '0644',
    target  => "/Users/${::luser}/github/dotfiles/zsh/zshrc",
    require => Repository["/Users/${::luser}/github/dotfiles"],
  }

  file  { "/Users/${::luser}/.oh-my-zsh/themes/sschuth.zsh-theme":
    ensure  => link,
    mode    => '0644',
    target  => "/Users/${::luser}/github/dotfiles/sschuth.zsh-theme",
    require => Repository["/Users/${::luser}/github/dotfiles"],
  }
  
}