class people::schuth::databases{
	# install dmg package for mysql
	package{ 'mysql':
		source => 'http://dev.mysql.com/get/Downloads/MySQL-5.6/mysql-5.6.21-osx10.9-x86_64.dmg',
		provider => 'pkgdmg'
	}
	# utilities for mysql
	package{ 'mysql-utilities':
		source => 'http://dev.mysql.com/get/Downloads/MySQLGUITools/mysql-utilities-1.5.2-osx10.7.dmg',
		provider => 'pkgdmg'
	}
	package{ 'mysql-workbench':
		source => 'http://dev.mysql.com/get/Downloads/MySQLGUITools/mysql-workbench-community-6.2.3-osx-i686.dmg',
		provider => 'pkgdmg'
	}
}