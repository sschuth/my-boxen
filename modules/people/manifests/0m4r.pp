class people::0m4r {
  #include people::0m4r::cli
  #include people::0m4r::databases
  #include people::0m4r::osxsettings
  include people::0m4r::applications
  #include people::0m4r::homebrew
  #include people::0m4r::dotfiles
  #include people::0m4r::privateconfiguration
  #include people::0m4r::gitsetup
  #include people::0m4r::lhprojectsetup
}