class people::0m4r::privateconfiguration {

  repository { "/Users/${::luser}/bitbucket/private-configuration":
    source => 'git@bitbucket.org:s0m4r/private-configuration.git',
    path => "/Users/${::luser}/bitbucket/private-configuration",
    provider => 'git',
  }

  $maven_settings = loadyaml("/Users/${::luser}/.maven-security")
  $nexus_username = $maven_settings[nexus][username]
  $nexus_password = $maven_settings[nexus][password]
  $security_master = $maven_settings[settingsSecurity][master]

  file { "/Users/${::luser}/.m2/":
    ensure => directory
  }

  file { "/Users/${::luser}/.m2/settings.xml":
    content => template("/Users/${::luser}/bitbucket/private-configuration/m2/settings.xml.erb"),
    require => Repository["/Users/${::luser}/bitbucket/private-configuration"],
    ensure => present
  }

  file { "/Users/${::luser}/.m2/settings-security.xml":
    content => template("/Users/${::luser}/bitbucket/private-configuration/m2/settings-security.xml.erb"),
    require => Repository["/Users/${::luser}/bitbucket/private-configuration"],
    ensure => present
  }

}