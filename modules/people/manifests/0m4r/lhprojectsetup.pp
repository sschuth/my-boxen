class people::0m4r::lhprojectsetup{
	#setup appdata as link to ~/appdata

	file { "/Users/${::luser}/lufthansa-project-data":
		ensure => directory
	}

	file { "/Users/${::luser}/lufthansa-project-data/appdata":
		ensure => directory
	}

	file { "/appdata":
    	ensure  => link,
    	mode    => '0644',
    	target  => "/Users/${::luser}/lufthansa-project-data/appdata",
  	}
}