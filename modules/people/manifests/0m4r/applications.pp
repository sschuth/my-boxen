class people::0m4r::applications{

  # Chrome
  include chrome::beta

  # from the stable channel
  #include chrome

  # from the beta channel
  #include chrome::beta

  # from the dev channel
  include chrome::dev

  # from the nightly channel
  include chrome::canary

  # FireFox
  include firefox

  # from a specific channel, name it directly
  include firefox::aurora


  # Sublime text
  include sublime_text

  sublime_text::package { 'Emmet':
    source => 'sergeche/emmet-sublime'
  }

  sublime_text::package { 'GitGutter':
    source => 'jisaacks/GitGutter'
  }

  sublime_text::package { 'Git':
    source => 'kemayo/sublime-text-git'
  }

  sublime_text::package { 'SublimeLinter':
    source => 'SublimeLinter/SublimeLinter-for-ST2'
  }

  sublime_text::package { 'SublimeLinter-jshint':
    source => 'SublimeLinter/SublimeLinter-jshint'
  }

  sublime_text::package { 'SublimeLinter-jscs':
    source => 'SublimeLinter/SublimeLinter-jscs'
  }



  # SourceTree
  # include sourcetree

  #Spotify
  # include spotify

  #iTerm 2
  include iterm2::stable
  include iterm2::colors::solarized_dark

  # skype
  include skype

  # evernote
  include evernote

}