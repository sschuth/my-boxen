class people::0m4r::gitsetup {
	git::config::global { 'alias.st':
		value => 'status'
	}
	git::config::global { 'alias.co':
		value => 'checkout'
	}
	git::config::global { 'alias.find':
		value => '!git ls-files | grep -i'
	}
	git::config::global { 'alias.review-last-merge':
		value => 'log -m --merges -p -1'
	}
	git::config::global { 'alias.ls-last-merge':
		value => 'log -m --merges --name-only -1'
	}
	git::config::global { 'user.name':
		value =>  'Omar Adobati'
	}
	git::config::global { 'user.email':
		value => 'oadobati@sapient.com'
	}
}