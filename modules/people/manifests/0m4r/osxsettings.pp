class people::0m4r::osxsettings {

  # =============
  # Keyboard
  # =============

  # map capslock to something useful
	# include osx::keyboard::capslock_to_control

  # map f1,f2 etc to their good old meaning (just because IntelliJ uses these ...)
  # include osx::global::enable_standard_function_keys

  # get rid of stupid autocorrect suggestions. I know how to txpe.
  include osx::global::disable_autocorrect

  # =============
  # Spaces and Dashboard
  # =============

  # turn off dashboard space
  boxen::osx_defaults { 'Disable dashboard':
  	user => $::boxen_user,
  	key => 'mcx-disabled',
  	domain => 'com.apple.dashboard',
  	value => true,
  }

  # =============
  # Dock
  # =============

  # move the dock to the right. Just because.
  class { 'osx::dock::position':
  	position => 'right'
  }

  # hide the dock if not in use to confuse users looking for it (it's right :)
  include osx::dock::autohide

  # =============
  # Trackpad
  # =============

  # make taps on the track pad act like a click
  include osx::global::tap_to_click

  # =============
  # Finder
  # =============

  # Turn on airdrop, for whatever reason it is turned off normally
  boxen::osx_defaults { 'Enable Airdrop':
    user => $::boxen_user,
    domain => 'com.apple.NetworkBrowser',
    key => 'DisableAirDrop',
    value => false
  }

  # restart finder
  include osx::finder

}