class people::schuth::applications{
	
	class { 'intellij':
  		edition => 'ultimate',
    	version => '13.1.5'
  }
  	
  include chrome::beta
  	
  include sublime_text

  include sourcetree

  include spotify
  
  include iterm2::stable
  include iterm2::colors::solarized_dark

  include launchbar

  include ohmyzsh
}